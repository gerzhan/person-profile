package ru.miroque.pp.domain;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PersonalityTest {
	Personality personality;
	
	@BeforeEach
	void init() {
		personality = new Personality(Sex.OTHER, "Foo");
	}

	@Test
	void testPersonality() {
		assertNotNull(personality);
	}

	@Test
	void testGetFIO() {
		assertEquals("Foo", personality.getFIO());
	}
	
	@Test
	void testChangeSex() {
		personality.setSex(Sex.FEMAIL);
		assertEquals(Sex.FEMAIL, personality.getSex());
	}

}
