package ru.miroque.pp.domain;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class NodeTest {

    @Test
    void testPPNodeOneSeed() {
        Seed seed = new DefaultSeed("miroque", null);
        Node root = new Node(seed, null, null);
        System.out.println(root.toString());
    }

    @Test
    void testPPNodeSubLevelSeed() {
        Seed seed = new DefaultSeed("miroque", null);
        Node root = new Node(seed, null, null);

        List<Node> _programmer = new ArrayList<>();

        Seed __javaSeed = new DefaultSeed("java", 80);
        Node __javaNode = new Node(__javaSeed, root, null);
        _programmer.add(__javaNode);

        root.setChildren(_programmer);

        System.out.println(root.toString());
    }

    @Test
    void testPPNodeSubSubLevelSeed() {
        Seed seed = new DefaultSeed("miroque", null);
        Node root = new Node(seed, null, null);

        List<Node> _programmer = new ArrayList<>();
        Seed __javaSeed = new DefaultSeed("java", null);

        List<Node> ___javaSE = new ArrayList<>();

        Node __javaNode = new Node(__javaSeed, root, ___javaSE);
        _programmer.add(__javaNode);

        Seed __javaSESeed = new DefaultSeed("java SE", 80);
        Node ___javaSENode = new Node(__javaSESeed, __javaNode, null);
        ___javaSE.add(___javaSENode);

        root.setChildren(_programmer);

        System.out.println("[simple]" + root.toString());
        System.out.println("[level root]" + root.getLevelOfNode());
    }

    @Test
    void testPPNodeSubSubLevelSeedTwo() {
        Seed seed = new DefaultSeed("miroque", null);
        Node root = new Node(seed, null, null);

        List<Node> _programmer = new ArrayList<>();
        Seed __javaSeed = new DefaultSeed("java", null);

        List<Node> ___javaSE = new ArrayList<>();

        Node __javaNode = new Node(__javaSeed, root, ___javaSE);
        _programmer.add(__javaNode);

        Seed __javaSESeed = new DefaultSeed("java SE", 80);
        Node ___javaSENode = new Node(__javaSESeed, __javaNode, null);
        ___javaSE.add(___javaSENode);
        Seed __javaSESeedFoo = new DefaultSeed("java SE FOO", 30);
        Node ___javaSENodeFoo = new Node(__javaSESeedFoo, __javaNode, null);
        ___javaSE.add(___javaSENodeFoo);

        root.setChildren(_programmer);

        System.out.println("[2 simple    ]" + root.toString());
        System.out.println("[2 level root]" + root.getLevelOfNode());
        System.out.println("[2 level java]" + __javaNode.getLevelOfNode());
    }

    @Test
    void testPPNodeSubTwoSubLevelSeedTwo() {
        Seed seed = new DefaultSeed("miroque", null);
        Node root = new Node(seed, null, null);

        // *************** PROGER
        List<Node> node_Java = new ArrayList<>();

        Seed seed_Programmer = new DefaultSeed("proger", null);
        Node node_Programmer = new Node(seed_Programmer, root, node_Java);

        Seed seed_Java_SE = new DefaultSeed("seed_Java_SE", 80);
        Node leaf_Java_SE = new Node(seed_Java_SE, node_Programmer, null);
        
        Seed seed_Java_EE = new DefaultSeed("seed_Java_EE", 30);
        Node leaf_Java_EE = new Node(seed_Java_EE, node_Programmer, null);
        
        node_Java.add(leaf_Java_SE);
        node_Java.add(leaf_Java_EE);

        // *************** ARTIST
        Seed seed_Artist = new DefaultSeed("artist", 15);
        Node leaf_Artist = new Node(seed_Artist, root, null);

        List<Node> subTreeOfSkills = new ArrayList<>();
        subTreeOfSkills.add(node_Programmer);
        subTreeOfSkills.add(leaf_Artist);

        root.setChildren(subTreeOfSkills);

        System.out.println("[3 simple    ]" + root.toString());
        System.out.println("[3 level root]" + root.getLevelOfNode());
        System.out.println("[3 level node_Programmer]" + node_Programmer.getLevelOfNode());
        System.out.println("[3 level leaf_Artist]" + leaf_Artist.getLevelOfNode());
    }
}
