package ru.miroque.pp.domain;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class DefaultSeed implements Seed {
	private String label;
	private Integer value;

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public Integer getValue() {
		return value;
	}

    @Override
    public String toString() {
        return "DefaultSeed [label=" + label + ", value=" + value + "]";
    }

}
