package ru.miroque.pp.domain;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author panov Класс для хранения информации в узле дерева... Нужен ли
 *         параметр, или сразу можно всё указать, или можно параметром
 *         передавать подготовленный класс, с лейблами и значениями, и весами?
 * 
 *         хм.. жалко сейчас не вижу...
 * @param <T>
 */
@AllArgsConstructor
@Getter
@Setter
public class Node implements Serializable {
    private static final long serialVersionUID = 1L;
    private Seed data;
    private Node parent;
    private List<Node> children;

    public Node() {
    }

    boolean isNode() {
        return !isLeaf() && !isRoot();
    }

    boolean isLeaf() {
        return children == null ? true : false;
    }

    boolean isRoot() {
        return parent == null ? true : false;
    }

    public Double getLevelOfNode() {
        Double result = 0.0;

        if (!isLeaf()) {
            int i = 0;
            double sum = 0.0;
            for (Node node : children) {
                sum += node.getLevelOfNode();
                i++;
            }
            result = sum / i;
        } else {
            if (data.getValue() != null) {
                result = data.getValue().doubleValue();
            }
        }

        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Node");
        sb.append(" [data=").append(data != null ? data : "NA").append(", parent=")
                .append((parent != null ? parent.getData().getLabel() : "root")).append(", children=")
                .append(children != null ? children.toString() : "NA").append("]");
        return sb.toString();
    }

}
