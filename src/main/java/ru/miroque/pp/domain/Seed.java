package ru.miroque.pp.domain;

public interface Seed {
	String getLabel();

	Integer getValue();
}
