package ru.miroque.pp.repos;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import lombok.Getter;
import ru.miroque.pp.domain.Personality;
import ru.miroque.pp.domain.Sex;

@ApplicationScoped
public class RepoPersonality {

	@Getter
	private List<Personality> personalities = new ArrayList<Personality>();

	@PostConstruct
	private void initList() {
		Personality _01 = new Personality(Sex.MALE, "Jone Doe");
		personalities.add(_01);
		Personality _02 = new Personality(Sex.MALE, "Иван Добрый");
		personalities.add(_02);
		Personality _03 = new Personality(Sex.FEMAIL, "Ксения Умильная");
		personalities.add(_03);
		Personality _04 = new Personality(Sex.FEMAIL, "Ольга Стрежная");
		personalities.add(_04);

	}

}
