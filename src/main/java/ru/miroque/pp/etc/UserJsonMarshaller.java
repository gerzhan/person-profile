package ru.miroque.pp.etc;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import ru.miroque.pp.domain.User;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class UserJsonMarshaller implements MessageBodyWriter<User> {

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type == User.class;
    }

    @Override
    public void writeTo(User t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType,
            MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
            throws IOException, WebApplicationException {
        JsonObject jsonObject = Json.createObjectBuilder().add("id", t.getId().toString())
                .add("data", t.getMap().getData() != null ? t.getMap().getData().toString() : "NA")
                .add("parent", t.getMap().getParent() != null ? t.getMap().getParent().getData().getLabel() : "root")
                .add("children", t.getMap().getChildren() != null ? t.getMap().getChildren().toString() : "NA").build();
        
               DataOutputStream outputStream = new DataOutputStream(entityStream);
               outputStream.writeBytes(jsonObject.toString());
    }

}
