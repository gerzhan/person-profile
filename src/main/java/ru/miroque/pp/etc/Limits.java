package ru.miroque.pp.etc;

import java.io.Serializable;

public class Limits implements Serializable {
    private static final long serialVersionUID = 1L;
    public int page; // e.g. 0 page means out of range
    public int pages;
    public int tableRowsCapacity; // e.g. 10, 20, 50, 100
    public int tableRowsTotal;

    public Limits() {
        super();
    };

    public Limits(int page, int pages, int tableRowsCapacity, int tableRowsTotal) {
        this.page = page;
        this.pages = pages;
        this.tableRowsCapacity = tableRowsCapacity;
        this.tableRowsTotal = tableRowsTotal;
    }
}
