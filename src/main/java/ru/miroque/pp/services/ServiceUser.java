package ru.miroque.pp.services;

import java.util.List;

import javax.inject.Inject;

import ru.miroque.pp.domain.User;
import ru.miroque.pp.etc.Limits;
import ru.miroque.pp.repos.RepoPersonality;
import ru.miroque.pp.repos.RepoUser;

public class ServiceUser {
	@Inject
	private RepoPersonality repoPersonality;

	@Inject
	private RepoUser repoUser;

	public User getUser(String id) {
		return repoUser.getUsers().get(Integer.parseInt(id));
	}

	public List<User> getChunk(Limits limits) {
		return repoUser.getUsers();
	}

}
